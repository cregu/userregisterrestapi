package com.cregu.springuserregisterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringUserRegisterApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringUserRegisterApiApplication.class, args);
    }

}
