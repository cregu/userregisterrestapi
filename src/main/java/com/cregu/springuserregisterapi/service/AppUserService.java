package com.cregu.springuserregisterapi.service;

import com.cregu.springuserregisterapi.model.AppUser;
import com.cregu.springuserregisterapi.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    public AppUser registerAppUser(AppUser toRegister) {
        AppUser created = appUserRepository.save(toRegister);

        return created;
    }
}
