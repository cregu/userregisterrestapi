package com.cregu.springuserregisterapi.controller;

import com.cregu.springuserregisterapi.model.AppUser;
import com.cregu.springuserregisterapi.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @PostMapping("/registerUser")
    public ResponseEntity<AppUser> registerUser(@RequestBody AppUser registerUser){
        AppUser registeredUser = appUserService.registerAppUser(registerUser);

        return ResponseEntity.ok(registeredUser);
    }
}
